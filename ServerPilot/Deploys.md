## Obtener la ruta de trabajo
Posicionarse en la ruta de trabajo
```sh
#!/bin/bash
# inicio=('date +"%s"')
alias php=/usr/bin/php7.4-sp
alias composer=/usr/bin/composer7.4-sp
php artisan down
git pull
git reset --hard
php artisan route:clear
composer install --no-dev
yarn install
yarn prod
php artisan route:cache
php artisan up
# fin='date +"%s"'
# tiempo=$(($fin-$inicio))
# echo "Se tomó $tiempo segundos"
```
