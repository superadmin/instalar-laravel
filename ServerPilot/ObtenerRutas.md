<!-- https://stackoverflow.com/questions/18648345/saving-awk-output-to-variable/18648441 -->

## Buscar la ruta de la última versión de PHP
```sh
ls /usr/bin/php* | sort -rn | head -n 1 | awk '{print $1}' # /usr/bin/php7.4-sp
```

Copiar la ruta y verificar
```sh
alias php=/usr/bin/php7.4-sp
php -v
```

## Buscar la ruta de composer
```sh
ls /usr/bin/composer* | sort -rn | head -n 1 | awk '{print $1}' # /usr/bin/composer7.4-sp
```

## Crear alias provisorias
```sh
alias composer=/usr/bin/composer7.4-sp
composer --version
```
