## Buscar la ruta de PHP
```sh
ls /usr/bin/php* | sort -rn | head -n 1 | awk '{print $1}' # /usr/bin/php7.4-sp
```

## Buscar la ruta de composer
```sh
ls /usr/bin/composer* | sort -rn | head -n 1 | awk '{print $1}' # /usr/bin/composer7.4-sp
```

## Crear alias provisorias
```sh
alias php=/usr/bin/php7.4-sp
alias composer=/usr/bin/composer7.4-sp
```

## Crear las ssh
```sh
ssh-keygen -t ed25519 -C "un_comentario"
```

## Copiar el contenido
```sh
cat ~/.ssh/id_ed25519.pub
```

## Obtener el "$directorio" de trabajo
```sh
cd ~/apps/ # (y tabular)
```

## Agregar las ssh en GitLab / GitHub
```js
// (Esto se hace directamente en la web gitlab.com/github.com)
```

```sh
$ = Cambiar
$directorio_app
```

## Eliminar carpeta public (y el index por default)
```
rm -Rf ~/apps/{$directorio_app}/public
```

## Copiar el proyecto
```sh
git clone {$url_proyecto} ~/apps/{directorio_app}
```

## Ubicarse en la ruta de trabajo y ejecutar composer
```sh
cd {$directorio_app}
composer install
```

## Copiar archivo .env y generar la clave
```sh
cp .env.example .env
php artisan key:generate
```

## Convertir en app en producción. Setear la DB y datos importantes
```sh
vi .env
APP_ENV=production
APP_DEBUG=false
APP_URL={$url}
```

## Ejecutar migraciones (y seeders, si es necesario)
```sh
php artisan migrate:fresh # --seed
```

## Instalar dependencias con Yarn
```sh
yarn install
```

## Producir con yarn
```sh
yarn prod
```
